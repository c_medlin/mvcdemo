package mvcstudy;

public class MVCstudy {


    /**
     * 
     * This starts the program.
     * We create a model and pass an iterator to the View
     */
    public static void main(String[] args) {
        
        Model m = new Model("resources/numbers.txt");
        View v = new View(m.iterator());

    }
    
}
