
package mvcstudy;

import gui.DrawPanel;
import gui.Drawable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import util.Random;

/**
 * The View in MVC is the GUI part. Here we set up a GUI.
 * The Controller in MVC is the part that says what to show in the GUI
 * based on user interaction.
 * 
 */
public class View extends gui.CenterFrame implements gui.Drawable{
    
    private BorderLayout bl;
    private GridLayout gl;
    private JPanel jp1, jp2;
    private JButton jbutton;
    private JList jl;
    private JComboBox jcb;
    private JScrollPane jsp;
    private ArrayList<String> data;
    private String disp_string; 
    
    private Random rng = Random.getRandomNumberGenerator(); //example of singleton
    
    public View(Iterator<String> d) {
        super(600, 100, "MVCDemo");
        data = new ArrayList<String>();
        while (d.hasNext()) {
            data.add(d.next());
        }
        
        bl = new BorderLayout();
        setLayout(bl);
        
        jp1 = new JPanel();
        add(jp1, BorderLayout.CENTER);
        
        DrawPanel draw = new DrawPanel();
        draw.setPreferredSize(new Dimension(200, 50));
        jp1.setBackground(Color.yellow);
        jp1.add(draw);
        
        jp2 = new JPanel();
        gl = new GridLayout(2, 1);
        
        add(jp2, BorderLayout.WEST);
        jp2.setLayout(gl);
        
        jbutton = new JButton();
        Controller c = new Controller();
        jbutton.addActionListener(c);
        jbutton.setText("Click to reverse order and change color!");
        jcb = new JComboBox<String>();
        for (String s : data) {
            jcb.addItem(s);
        }
        
        jp2.add(jbutton);
        jp2.add(jcb);

        jsp = new JScrollPane();
        
        jl = new JList();
        String[] array_of_strings = {"abc" , "def", "ghi", "jkl", "mno", "pqr"};
        
        jl.setListData(array_of_strings);
        
        jsp.getViewport().add(jl); //copied this from the mp3 player
        jsp.setPreferredSize(new Dimension(75, 50));
        add(jsp, BorderLayout.EAST);
        
        
        draw.setDrawable(this);
        setVisible(true);
        
    }

    private int times_drawn = 0;
    @Override
    public void draw(Graphics g, int width, int height) {
        System.out.println("I am in draw() " + times_drawn++);
  
    }

    @Override
    public void mouseClicked(int x, int y) {
        System.out.println("X location " + x + " Y location " + y);
        jp1.setBackground(newRandomColor());
    }

    @Override
    public void keyPressed(char key) {

    }
    
    private Color newRandomColor() {
        int alpha = rng.randomInt(0, 255);
        int red = rng.randomInt(0, 255);
        int green = rng.randomInt(0, 255);
        int blue = rng.randomInt(0, 255);
        return new Color (red, green, blue, alpha);
    }
    private class Controller implements ActionListener {
    
        private int count = data.size();
        private boolean ascending = true;
        public Controller() {
            
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            java.util.Stack<String> s = new java.util.Stack<>();
            jcb.removeAllItems(); //removes all items in jcombobox
            s.clear(); //removes all items in stack
            if (ascending) {
                for (int i = 0; i < count; i++) {
                    s.push(data.get(i));
                }

                while (!s.isEmpty()) {
                    jcb.addItem(s.pop());
                }
            } else {
                for (int i = 0; i < count; i++) {
                    s.push(data.get(i));
                    jcb.addItem(s.pop());
                }
            }
            ascending = !ascending;
            
            jp1.setBackground(newRandomColor());

            //repaint();
        }
    }
}
