
package mvcstudy;

import util.ReadTextFile;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * The Model in MVC only deals with the data.
 * In this case all we do is read in data and store it.
 * 
 * We also use our favorite design pattern used to get the data.
 * 
 */
public class Model {
    
    private util.ReadTextFile rtf;
    private ArrayList<String> mydata;
    public Model(String s) {
        rtf = new ReadTextFile(s);
        mydata = new ArrayList<String>();
        readData();
    }

    private void readData() {
        String s = rtf.readLine();
        while (!rtf.EOF()) {
            mydata.add(s);
            
            s = rtf.readLine();
        }
        rtf.close();
    }
    
    Iterator<String> iterator() {
        return mydata.iterator();
    }
}
